Pod::Spec.new do |spec|
  spec.name = "Tracking"
  spec.version = "1.0.0"
  spec.summary = "Tracking module for PwC."
  spec.license = { type: 'PwC', file: 'LICENSE' }
  spec.authors = { "Birgit Van Keer" => 'birgit.vankeer@flowpilots.com' }
  spec.homepage = "https://birgit.vankeer@stash.flowpilots.com/scm/pwc/pwc-mytaxlocator-sdk-touch.git"
  spec.platform = :ios, "9.0"
  spec.requires_arc = true
  spec.source = { git: "https://birgit.vankeer@stash.flowpilots.com/scm/pwc/pwc-mytaxlocator-sdk-touch.git", tag: "v#{spec.version}", submodules: true }
  spec.source_files = "TrackingModule/Tracking/**/*.{h,swift}"
  spec.pod_target_xcconfig = { 'SWIFT_VERSION' => '3' }
  spec.resource_bundle = { 'TrackingLibrary' => [ 'TrackingModule/Tracking/**/*.json' ] }

  spec.dependency "Alamofire"
  spec.dependency "ObjectMapper"
  spec.dependency "FXKeychain"
  spec.dependency "AFDateHelper"
end
